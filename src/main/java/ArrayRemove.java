public class ArrayRemove {

    public static void main(String[] args) {

        int[] myArray = populateArray();
        myArray = removeFromPoint(myArray, 3);
        printArray(myArray);



    }

    private static void printArray(int[] myArray) {

        for(int i=0;i<myArray.length;i++)
        {
            System.out.println(myArray[i]+"\n");
        }
    }

    private static int[] removeFromPoint(int[] myArray, int i) {

        int pointerValue = i-1;

        for(int count=pointerValue;count<myArray.length-1;count++)
        {
            myArray[count] = myArray[count+1];
        }

        myArray[myArray.length-1] = 0;

        return myArray;
    }

    private static int[] populateArray() {
        int[] array = new int[5];

        array[0] = 1;
        array[1] = 2;
        array[2] = 3;
        array[3] = 4;
        array[4] = 5;

        return array;
    }
}
