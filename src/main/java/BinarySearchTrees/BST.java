package BinarySearchTrees;

import java.util.ArrayList;
import java.util.List;

public class BST
{

    private Node root;
    private List<Node> myList;

    public BST()
    {
        root = null;
    }

    public Node insert(int value)
    {
        return root = insert(value, root);
    }

    public Node delete(int value)
    {
        return root = delete(value,root);
    }

    private Node delete(int value, Node root) {

        if(value < root.getValue())
        {
            root.nodeLeft = delete(value, root.getNodeLeft());
        }
        else if(value > root.getValue())
        {
            root.nodeRight = delete(value, root.getNodeRight());
        }

        else
        {
            if(root.nodeLeft == null)
            {
                return root.nodeRight;
            }
            else if(root.nodeRight == null)
            {
                return root.nodeLeft;
            }

            // more then 2 values.
            // find the smallest value on the right most subtree

            root.value = minValue(root.getNodeRight());

            // delete the value you just used to replace

            delete(root.value, root.getNodeRight());

        }
       return root;
    }

    private int minValue(Node nodeRight) {

        int value = nodeRight.getValue();
        Node smallestNodeOnRight = nodeRight;

        while (smallestNodeOnRight != null) {
            value = smallestNodeOnRight.getValue();
            smallestNodeOnRight = smallestNodeOnRight.getNodeLeft();
        }
        return value;
    }

    private Node insert(int value, Node root) {

        if(root == null)
        {
            Node newNode = new Node(value);
            return newNode;
        }

        if(value < root.getValue())
        {
            root.nodeLeft = insert(value, root.getNodeLeft());
        }
        else if(value > root.getValue())
        {
            root.nodeRight = insert(value, root.getNodeRight());
        }

        return root;
    }

    public void inorder()
    {
        inorder(root);
    }

    private void inorder(Node root) {

        if(root!=null) {
            inorder(root.getNodeLeft());
            System.out.println(root.getValue());
            inorder(root.getNodeRight());
        }
    }

    // create a traversal for breadth
    // logic: add all the nodes to a queue

    public int height()
    {
        int height = height(root);
        return height;
    }

    public void printLevelOrder()
    {
        int h = height(root);
        int i;
        for (i=1; i<=h; i++)
            printGivenLevel(root, i);
    }

    private int height(Node root)
    {
        if (root == null)
            return 0;
        else
        {
            /* compute  height of each subtree */
            int lheight = height(root.nodeLeft);
            int rheight = height(root.nodeRight);

            /* use the larger one */
            if (lheight > rheight)
                return(lheight+1);
            else return(rheight+1);
        }
    }

    void printGivenLevel (Node root ,int level)
    {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.value + " ");
        else if (level > 1)
        {
            printGivenLevel(root.nodeLeft, level-1);
            printGivenLevel(root.nodeRight, level-1);
        }
    }


    private class Node
    {
        private int value;
        private Node nodeLeft, nodeRight;

        private Node() {
        }

        private Node(int value)
        {
            this.value = value;
            nodeLeft = null;
            nodeRight = null;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNodeLeft() {
            return nodeLeft;
        }

        public Node getNodeRight() {
            return nodeRight;
        }

        public void setNodeRight(Node nodeRight) {
            this.nodeRight = nodeRight;
        }
    }
}