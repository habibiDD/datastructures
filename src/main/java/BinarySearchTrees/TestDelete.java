package BinarySearchTrees;

public class TestDelete {

    public static void main(String[] args) {


        BST tree = new BST();

        /* Let us create following BST
              50
           /     \
          30      70
         /  \    /  \
        20   40  60   80 */
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(2);
        tree.insert(4);

        tree.inorder();

        int height = tree.height();

        System.out.println(height);


    }
}
