package BinarySearchTrees;

public class TestTraversals {

    public static void main(String[] args) {


        BST tree = new BST();

        /* Let us create following BST
              4
           /     \
          2       6
         /  \    /  \
        1   3    5   7*/
        tree.insert(4);
        tree.insert(6);
        tree.insert(2);
        tree.insert(3);
        tree.insert(1);
        tree.insert(7);
        tree.insert(5);

        tree.inorder();

        System.out.println("Level order print");

        tree.printLevelOrder();




    }

}
