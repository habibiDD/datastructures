package LinkedList;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkListInsert {
    Node header = new Node("header");
    public static void main(String[] args) {

    }

    public Node searchForNode(String value)
    {
        Node foundNode = null;

        Node pointer = header;

        while(pointer.getNode()!=null)
        {
            if(pointer.getValue().equalsIgnoreCase(value))
            {
                foundNode = pointer;
            }
        }
        return foundNode;
    }


    private class Node
    {
        private String value;
        private Node Node;

        public Node(String value) {
            this.value = value;
            Node = null;
        }


        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Node getNode() {
            return Node;
        }

        public void setNode(Node node) {
            Node = node;
        }
    }
}
