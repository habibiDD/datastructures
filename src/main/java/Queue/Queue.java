package Queue;

import java.util.ArrayList;
import java.util.List;

public class Queue {

    private Node header, tail = null;
    int counter = 0;
    List<Node> q = new ArrayList<Node>();

    // when we add, create a new node, set the tails next value to that
    public void enqueue(int value)
    {
        Node newNode = new Node(value);
        if(q.size()==0)
        {
            tail = newNode;
            header = newNode;
        }
        else {
            tail.setNode(newNode); //
            tail = newNode;
        }
        q.add(newNode);
    }

    public Node dequeue()
    {
        if(!q.isEmpty()) {
            Node getNode = q.remove(0);
            header = header.getNode();
            return getNode;
        }
        throw new RuntimeException();
    }

    public void printAll()
    {
        for(int i=0;i<q.size();i++)
        {
            System.out.println(q.get(i).getValue()+"\n");
        }
    }

    private class Node
    {
        private Node node;
        private int value;

        public Node(int value) {
            this.node = null;
            this.value = value;
        }

        public Node getNode() {
            return node;
        }

        public void setNode(Node node) {
            this.node = node;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
