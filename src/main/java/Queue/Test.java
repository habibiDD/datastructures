package Queue;
// Driver class

public class Test {
    public static void main(String[] args)
    {
        Queue q = new Queue();
        q.dequeue();
        q.enqueue(10);
        q.enqueue(20);
        q.dequeue();
        q.dequeue();
        q.enqueue(30);
        q.enqueue(40);
        q.enqueue(50);
        q.printAll();

        //System.out.println("Dequeued item is " + q.dequeue().getValue());
    }
}
