package StackUsingLinkedList;

import org.xml.sax.ErrorHandler;

import java.util.ArrayList;
import java.util.List;

public class StackUsingLinkedList {
    
    List<Node> stack = new ArrayList();
    Node header = new Node();

    public void push(int value)
    {
        Node newNode = new Node(value);
        header = newNode;
        stack.add(newNode);
    }

    public void display()
    {
        for(int i=0;i<stack.size();i++)
        {
            System.out.println(stack.get(i).getValue()+"\n");
        }
    }

    public int peek()
    {
        return header.getValue();
    }

    public void pop()
    {
        stack.remove(header);
        int size = stack.size();
        header = stack.get(size-1);
    }
    

    private class Node
    {
        private Node node;
        private int value;

        public Node(int value) {
            this.node = null;
            this.value = value;
        }

        public Node() {
            this.value = 0;
            this.node = null;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNode() {
            return node;
        }

        public void setNode(Node node) {
            this.node = node;
        }
    }

}
